package cocus.flightinportugal;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cocus.flightinportugal.datamodel.Request;
import cocus.flightinportugal.datamodel.RequestRepository;
import cocus.flightinportugal.msgmodel.ListReply;
import cocus.flightinportugal.msgmodel.Reply;
import cocus.flightinportugal.skypicker.msgmodel.Datum;
import cocus.flightinportugal.skypicker.msgmodel.Search;
import cocus.flightinportugal.utils.Averager;
import cocus.flightinportugal.utils.DateUtils;
import cocus.flightinportugal.utils.MathUtils;

@RestController
@EnableMongoRepositories
public class FlightInPortugalController
{
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	RequestRepository mongoDBRepository;
	
	DataPersistenceHandler dataPersistenceHandler;
	  
	private static final Logger log = LoggerFactory.getLogger(FlightInPortugalController.class);
	
	private static final String baseURL = "https://api.skypicker.com/flights?";

	/**
	 * Method for the initialisation of the necessary provisions for the operation of the backend
	 * 
	 */
	@PostConstruct
	private synchronized void initialize()
	{
		dataPersistenceHandler = new DataPersistenceHandler(mongoDBRepository);
	}
	
	/**
	 * Handles the requests for the listing of all flight search requests on record
	 * 
	 * @return a ListReply (JSON) object which contains an initial message and a list of Request (JSON) objects, each of which
	 * represents a flight search request sent to the database
	 */
	@GetMapping("/flght/list")
	public ListReply listRecords()
	{
		//log the reception of request
		log.info("A request for search request records was received");
		
		//retrieve request info from MongoDB
		List<Request> reqs = dataPersistenceHandler.getAllSearchRequests();
		log.debug("A total of " + ((reqs == null) ? 0 : reqs.size()) + " records was retrieved");
		
		//build the response object
		ListReply listReply = new ListReply("A toltal of " + reqs.size() + " records in store.");
		listReply.setRequests(reqs);
		
		return listReply;
	}

	/**
	 * Handles the requests for the deletion of all flight search requests on record
	 * 
	 * @return a ListReply (JSON) object which contains an initial message indicating that no flight search requests are on record
	 */
	@GetMapping("/flght/del")
	public ListReply deleteRecords()
	{
		//log the reception of request
		log.info("A request for the removal of all search request records was received");
		
		dataPersistenceHandler.deleteAllSearchRequests();
		List<Request> reqs = dataPersistenceHandler.getAllSearchRequests();
		ListReply listReply;
		if(reqs==null || reqs.size()<1)
		{
			listReply = new ListReply("No records in store.");
			log.debug("Records sucessfuly removed");
		}
		else
		{
			listReply = new ListReply("Not all records were removed!!.");
			log.error("Not all records were removed");
		}
		listReply.setRequests(reqs);
		
		return listReply;
	}
	
	/**
	 * Calculates the average price of the flights between Oporto and Lisbon (as well as for the first and second bags in such flights).
	 * This calculation may be conditioned to a specific time interval or to a specific destination (either Porto or Lisbon)
	 * 
	 * @param dateFrom the beginning date of the time interval in scope  
	 * @param dateTo the ending date of the time interval in scope
	 * @param destinations the list of target destinations of the flights to employ in the calculation (either Oporto, Lisbon or both)
	 * @param currency the currency in which the values in scope should be presented
	 * @return a Reply (JSON) object the desired information
	 */
	@GetMapping("/flght/avg")
	public Reply avg(
			@RequestParam(value = "dateFrom", required = false) String dateFrom,
			@RequestParam(value = "dateTo", required = false) String dateTo,
			@RequestParam(value = "dest", required = false) List<String> destinations,
			@RequestParam(value = "curr", defaultValue = "EUR") String currency
			)
	{
		
		//log the reception of request
		log.info("A request for the average information was received");
		
		Reply reply = null;
		long reqID = getID();

		//if no destination is provided the information is retrieved for both Oporto->Lisbon and Lisbon->Oporto flights
		if(destinations == null || destinations.size()<1)
		{
			log.info("No destinations were provided. These will be considered to be both Oporto and Lisbon");
			destinations = new ArrayList<String>();
			destinations.add("OPO");
			destinations.add("LIS");
		}
		
		//validate the input arguments
		StringBuilder resultMsg = new StringBuilder();
		if(!validateParameters(dateFrom,dateTo,destinations,currency,resultMsg))
		{
			log.error("One or more of the provided arguments is inadequately expressed.");
			reply = new Reply(reqID, resultMsg.toString()); 
		}
		else
		{
			try
			{
				log.debug("Receiving request for data on flights from date " + dateFrom + " to date= " + dateTo + " with prices in " + currency);
				
				//persist request information to MongoDB
				dataPersistenceHandler.persistRequestInfo(reqID, dateFrom,dateTo,destinations,currency);
				
				//normalise arguments
				destinations = normalizeCityNames(destinations);
				
				//build search query(ies)
				HashMap<String,String> searchQueries = buildSearchQueries(baseURL,destinations,dateFrom,dateTo,currency);
				//DebugUtils.printIterableList(searchQueries.entrySet(),"SearchQueries");
				
				//retrieve data from skypicker service 
				log.debug("Retrieving information form https://api.skypicker.com");
				HashMap<String,Object> searchResults = querySkypicker(searchQueries);
				
				//save the search results to file(s)
				//DebugUtils.saveSearchResults(searchResults,"C:/Users/Helder/Desktop/");
				
				log.debug("Processing Skypicker's response and building the reply msg");
				reply = buildReply(searchResults,reqID,dateFrom, dateTo,currency);				
			}
			catch (Exception e)
			{
				log.error("An error occurred while handling the request");
				reply = new Reply(reqID, "An error occurred while handling the request" + e.getMessage()); 
			}
		}
		
		return reply;
	}

	/**
	 * Validates the adequateness of the parameters received from the client side
	 * 
	 * @param dateFrom the beginning date of the time interval in which the average prices are to be calculated 
	 * @param dateTo the ending date of the time interval in which the average prices are to be calculated 
	 * @param destinations the target destination of the flights to employ in the calculation (either Oporto or Lisbon)
	 * @param currency the currency in which the values in scope should be presented
	 * @param results the StringBuilder object which enables returning (to the caller side) the error describing explanation, if one occurs 
	 * @return true if all the given parameters are valid
	 */
    private boolean validateParameters(String dateFrom, String dateTo, List<String> destinations, String currency, StringBuilder results)
	{
		boolean valid = true;
		
		long fromDateLong = 0;
		long toDateLong = 0;
		
		if(dateFrom!=null)
		{
			try {fromDateLong = DateUtils.dateStringToTimeInMillis(dateFrom);}
			catch (ParseException e)
			{
				results.append("The dateFrom parameter contains an inadequate value " + dateFrom + ".");
				valid = false;
			}
		}
		
		if(dateTo!=null)
		{
			try
			{
				toDateLong = DateUtils.dateStringToTimeInMillis(dateTo);
			}
			catch (ParseException e)
			{
				results.append("The dateTo parameter contains an inadequate value " + dateTo + ".");
				valid = false;
			}
		}
	
		if(valid && (toDateLong!=0 && fromDateLong!=0) && (toDateLong<=fromDateLong))
		{
			results.append("The specified toDate (" + dateTo + ") is earlier or the same as the specified fromDate (" + dateFrom + ").");
			valid = false;
		}
		
		if(destinations.size()<1 || destinations.size()>2)
		{
			results.append("Inadequate number of destinations specified (" + destinations.size() + ").");
			valid = false;
		}
		else
		{
			Iterator<String> destinationIterator = destinations.iterator();
			while(destinationIterator.hasNext())
			{
				String destination = destinationIterator.next();
				if(!isEitherOportoOrLisbon(destination))
				{
					results.append("One of the specified destination city(ies) (" + destination + ") is neither Porto nor Lisbon.");
					valid = false;
					break;
				}
			}
		}
		
		return valid;
	}

    /**
	 * Receives a list (1 or 2 values long) of flight destinations {Lisbon,LIS,Lisboa,LPPT or Oporto,OPO,Porto,LPPR}
	 * and converts these to their normalised values within this application {LIS, OPO}
	 * 
	 * 
	 * @param destinations the list of flight destinations
	 * @return a list with the normalised designations of the flight destinations
	 */
	private List<String> normalizeCityNames(List<String> destinations)
    {
    	List<String> normalizedDests = new ArrayList<String>(); 
		Iterator<String> destinationIterator = destinations.iterator();
		while(destinationIterator.hasNext())
		{
			String destCity = destinationIterator.next();
			if(isLisbon(destCity)) {destCity="LIS";}
			else if(isOporto(destCity)) {destCity="OPO";}
			normalizedDests.add(destCity);
		}
		return normalizedDests;
	}

	/**
	 * Builds the full URLs (including the query parameters) to be employed in the contacting and querying of the Skypicker service.
	 * Depending on the specified list of destinations (coming from the client side) it may include 1 or 2 URLs. 
	 * 1 such URL for each of the target destinations. 
	 * 
	 * @param baseURL the base URL of the Skypicker service
	 * @param destinations the list of flight destinations
	 * @param fromDate the beginning date of the time interval for the search request in scope
	 * @param toDate the ending date of the time interval for the search request in scope
	 * @param currency the currency in which the values in scope should be presented back to the client side
	 * 
	 * @return a HashMap<String,String> object whose keys are the flight destinations and whose values are the associated query URL 
	 */
	private HashMap<String,String> buildSearchQueries(String baseURL, List<String> destinations, String fromDate, String toDate, String currency)
	{
		HashMap<String,String> searchQueries = new HashMap<String,String>();
		
		Iterator<String> destinationIterator = destinations.iterator();
		while(destinationIterator.hasNext())
		{
			String destinationCity = destinationIterator.next();
	
			searchQueries.put(destinationCity,buildSearchQuery(baseURL,destinationCity, fromDate, toDate, currency));
		}
		
		return searchQueries;
	}

	/**
	 * Builds the full URL (including the query parameters) to be employed in the contacting and querying of the Skypicker service
	 * for information pertaining to flights to one specific destination. 
	 * 
	 * @param baseURL the base URL of the Skypicker service
	 * @param destinationCity the flight destinations city
	 * @param fromDate the beginning date of the time interval for the search request in scope
	 * @param toDate the ending date of the time interval for the search request in scope
	 * @param currency the currency in which the values in scope should be presented back to the client side
	 * 
	 * @return the Skypicker querying URL 
	 */
	private String buildSearchQuery(String baseURL, String destinationCity, String fromDate, String toDate, String currency)
	{		
		StringBuilder searchQueryBuilder = new StringBuilder(baseURL);
		
		String departureCity = "LIS";
		if(isLisbon(destinationCity)) {destinationCity="LIS";departureCity="OPO";}
		else if(isOporto(destinationCity)) {destinationCity="OPO";}
	
		searchQueryBuilder.append("flyFrom=" + departureCity + "&to=" + destinationCity);
		if(fromDate!=null) {searchQueryBuilder.append("&dateFrom=" + fromDate);}
		if(toDate!=null) {searchQueryBuilder.append("&dateTo=" + toDate);}
		searchQueryBuilder.append("&partner=picky");
		if(currency!=null) {searchQueryBuilder.append("&curr=" + currency);}
		
		String searchQuery = searchQueryBuilder.toString();
		log.debug("The search query/URL " + searchQuery  + " was produced");
		return searchQuery;
	}

	/**
	 * Receives a set of Skypicker querying URLs, reaches out to Skypicker though those URLS and receives that service's response, which it returns.
	 * 
	 * @param searchQueries a HashMap<String,String> object whose keys are the flight destinations and whose values are the associated query URL
	 * @return a HashMap<String,String> object whose keys are the flight destinations and whose values are Skypicker's replies to the 
	 * queries associated to those key in the input  HashMap object
	 */
	private HashMap<String,Object> querySkypicker(HashMap<String,String> searchQueries)
    {
		HashMap<String,Object> searchResults = new HashMap<String,Object>();

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Accept", "application/json");
		HttpEntity<String> httpEntity = new HttpEntity<String>(httpHeaders);

	    Iterator<Entry<String,String>> searchQueryIterator = searchQueries.entrySet().iterator();
	    while (searchQueryIterator.hasNext())
	    {
	        Map.Entry<String,String> pair = (Map.Entry<String,String>) searchQueryIterator.next();
	        String destName = pair.getKey();
	        String searchQuery = pair.getValue();
	        
	    	try
	    	{
	    		ResponseEntity<String> response = restTemplate.exchange(searchQuery,HttpMethod.GET, httpEntity, String.class);
			    if (response.getStatusCode().equals(HttpStatus.OK))
			    {
			    	log.debug("Skypicker service successfully queried in regards to flights with destination " + destName);
					Search searchResult = new ObjectMapper().readValue(response.getBody(), Search.class);
					searchResults.put(destName,searchResult);
			    }
			    else
			    {
			    	log.warn("An error occurred in the querying of the Skypicker service in regards to flights with destination " + destName);
			    	searchResults.put(destName,response.getBody());
			    }
			}
	    	catch (HttpClientErrorException e)
	    	{
	    		searchResults.put(destName,e.getResponseBodyAsString());
			}
	    	catch (JsonMappingException e)
	    	{
	    		log.error("Exception occurred processing Skypicker's response " + e.getMessage());
				searchResults.put(destName,null);
			}
	    	catch (JsonProcessingException e)
	    	{
	    		log.error("Exception occurred processing Skypicker's response " + e.getMessage());
				searchResults.put(destName,null);
			}
	    }
		
    	return searchResults;
	}

	/**
	 * Builds the Reply (JSON) object to be sent back to the client side
	 * 
	 * @param searchResults a HashMap<String,String> object whose keys are the flight destinations and whose values are Skypicker's replies to the 
	 * queries associated to those keys.
	 * @param id the identifier attributed to the search request
	 * @param fromDate the beginning date of the time interval for the search request in scope
	 * @param toDate the ending date of the time interval for the search request in scope
	 * @param currency the currency in which the values in scope should be presented back to the client side
	 * @return the Reply object
	 */
	private Reply buildReply(HashMap<String,Object> searchResults, long id, String fromDate, String toDate, String currency)
	{
		Reply reply = new Reply(id);
		
	    Iterator<Entry<String,Object>> searchResultIterator = searchResults.entrySet().iterator();
	    while (searchResultIterator.hasNext())
	    {
	        Map.Entry<String,Object> pair = (Map.Entry<String,Object>) searchResultIterator.next();
	        String destName = pair.getKey();
	        Object result = pair.getValue();
	        
	        //a reply was successfully received from Skypicker
	        if(result!=null && result instanceof Search)
	        {
		        Search searchResult = (Search) result;
				List<Datum> dataList = searchResult.getData();
				
			    List<Integer> flightPrices = new ArrayList<Integer>();
			    List<Double> frstBagPrices = new ArrayList<Double>();
			    List<Double> scndBagPrices = new ArrayList<Double>();
				
			    long fromDateLong = 0;
			    long toDateLong = 0;
			    
			    int targetFlightCounter = 0;
				Iterator<Datum> dataListIterator = dataList.iterator();
				while(dataListIterator.hasNext())
				{
					Datum datum = dataListIterator.next();
					List<String> airlines = datum.getAirlines();
					if(isOneOfTheAirlinesEitherTAPorRyanair(airlines))
					{
						targetFlightCounter = targetFlightCounter + 1;
						
						Integer flightPrice = datum.getPrice();
						if(flightPrice!=null) {flightPrices.add(flightPrice);}
						//else {log.info("Null value found for flightPrice at " + datum.getId());}
						
						Double frstBagPrice = datum.getBagsPrice().get1();
						if(frstBagPrice!=null) {frstBagPrices.add(frstBagPrice);}
						//else {log.info("Null value found for frstBagPrice at " + datum.getId());}
						
						Double sndBagPrice = datum.getBagsPrice().get2();
						if(sndBagPrice!=null) {scndBagPrices.add(sndBagPrice-frstBagPrice);}
						//else {log.info("Null value found for sndBagPrice at " + datum.getId());}
						
						if(fromDate==null)
						{
							long time = datum.getDTime()*1000L;
							if(fromDateLong==0 || time<fromDateLong){fromDateLong=time;}
						}
						if(toDate==null)
						{
							long time = datum.getDTime()*1000L;
							if(toDateLong==0 || time>toDateLong){toDateLong=time;}
						}
					}
				}
				
				log.debug("Information was retrieved on " + dataList.size() + " flights headed for " + destName + ". " + targetFlightCounter + " of these where from TAP or Ryanair");
				log.debug("A total of " + flightPrices.size() + " flight prices, " + frstBagPrices.size() + " first bag prices, and " + scndBagPrices.size() + "second bag prices were retrieved.");

				Integer avgFlightPrice = (Integer) (new Averager<Integer>(flightPrices).getAverage());
				Integer avgFrstBagPrice = MathUtils.round((Double) new Averager<Double>(frstBagPrices).getAverage());
				Integer avgScndBagPrice = MathUtils.round((Double) new Averager<Double>(scndBagPrices).getAverage());
				
				if(fromDate==null){fromDate=DateUtils.timeInMillis2DateString(fromDateLong);}
				if(toDate==null){toDate=DateUtils.timeInMillis2DateString(toDateLong);}
				
				if(destName.equals("OPO")) {reply.setOPO(avgFlightPrice, avgFrstBagPrice, avgScndBagPrice, fromDate, toDate, "Francisco Sa Carneiro", currency);}
				else {reply.setLIS(avgFlightPrice, avgFrstBagPrice, avgScndBagPrice, fromDate, toDate, "Humberto Delgado", currency);}
	        }
	        else //an error msg was received from Skypicker
	        {
	        	String msg = null;
	        	if(result!=null) {msg=(String) result;}
	        	else {msg="An error occurred in the Skypicker service or in the interaction with it.";}
	        	
				if(destName.equals("OPO")) {reply.setOPO(msg);}
				else {reply.setLIS(msg);}
	        }
	    }
	
		return reply;
	}

	/**
	 * Determines (within certain limits), from the given city name if it is Oporto or Lisbon (true) or none (false)
	 * 
	 * @param city the city name 
	 * @return true if the city in scope is either OportoOrLisbon
	 */
	private boolean isEitherOportoOrLisbon(String city)
	{
		boolean result = false;
		
		if(isOporto(city) || isLisbon(city)) {result = true;}
		
		return result;
	}


	/**
	 * Determines (within certain limits), if the specified city is Lisbon
	 * 
	 * @param city the city name
	 * @return true if the named city corresponds to Lisbon
	 */
	private boolean isLisbon(String city)
	{
		boolean result = false;
		if(city.equalsIgnoreCase("Lisbon") || city.equalsIgnoreCase("LIS") || city.equalsIgnoreCase("Lisboa") || city.equalsIgnoreCase("LPPT")){result=true;}
		return result;
	}

	/**
	 * Determines (within certain limits), if the specified city is Oporto
	 * 
	 * @param city the city name
	 * @return true if the named city corresponds to Oporto
	 */
	private boolean isOporto(String city)
	{
		boolean result = false;
		if(city.equalsIgnoreCase("Oporto") || city.equalsIgnoreCase("OPO") || city.equalsIgnoreCase("Porto") || city.equalsIgnoreCase("LPPR")){result=true;}
		return result;
	}


	/**
	 * Determines if the given (IATA or ICAO) code pertains to either TAP or Ryanair
	 * 
	 * @param airlines a list of airline codes
	 * @return true if at least one of the given airline codes pertains to TAP or Ryanair 
	 */
	private boolean isOneOfTheAirlinesEitherTAPorRyanair(List<String> airlines)
	{
		boolean result = false;
		Iterator<String> airlinesIterator = airlines.iterator();
		while(airlinesIterator.hasNext())
		{
			String airlineCode = airlinesIterator.next();
			if(isTAPCode(airlineCode.trim()) || isRyanairCode(airlineCode.trim())) 
			{
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * Determines if the given (IATA or ICAO) code pertains to TAP
	 * 
	 * @param code the given IATA or ICAO code
	 * @return true if the given code pertains to TAP
	 */
	private boolean isTAPCode(String code) 
	{
		boolean result = false;
		
		String IATA_Code = "TP";
		String ICAO_Code = "TAP";
		String Prefix_Code = "047";
		
		if(IATA_Code.equals(code) || ICAO_Code.equals(code) || Prefix_Code.equals(code)) {result=true;}
		
		return result;
	}
	
	/**
	 * Determines if the given (IATA or ICAO) code pertains to Ryanair
	 * 
	 * @param code the given IATA or ICAO code
	 * @return true if the given code pertains to Ryanair
	 */
	private boolean isRyanairCode(String code) 
	{
		boolean result = false;
		
		String IATA_Code = "FR";
		String ICAO_Code = "RYR";
		
		if(IATA_Code.equals(code) || ICAO_Code.equals(code)) {result=true;}
		
		return result;
	}

	/**
	 * Produces the identifier to be attributed to a specific flight search request.
	 *  
	 * @return the identifier to be attributed to a specific flight search request
	 */
	private static synchronized long getID(){return System.currentTimeMillis();}
	
}