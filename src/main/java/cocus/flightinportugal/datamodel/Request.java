package cocus.flightinportugal.datamodel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Request class holds the information pertaining to a specific flight search request 
 * Request (JSON) objects are persisted to a MongoDB, and are also sent to the client side in reply to requests for 
 * flight search request records  
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Request
{
	@Id
	@JsonProperty("id")
	public long id;
	
	@JsonProperty("dateFrom")
	String dateFrom;
	
	@JsonProperty("dateTo")
	String dateTo;
	
	@JsonProperty("destinations")
	List<String> destinations;
	
	@JsonProperty("currency")
	String currency;
	
	public Request() {}
	
	/**
	 * Constructor
	 * 
	 * @param id the identifier attributed (by the controller logic) to the flight search request
	 * @param dateFrom the beginning date of the time interval in which flights are of interest
	 * @param dateTo the end date of the time interval in which flights are of interest
	 * @param destA the first target destination of relevance (Oporto or Lisbon)
	 * @param destB the second target destination of relevance (Oporto or Lisbon)
	 * @param currency the currency in which the monetary values comprised in the results should be expressed 
	 */
	public Request(long id, String dateFrom, String dateTo, String destA, String destB, String currency)
	{
		this.id = id;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		if(destA!=null || destB!=null)
		{
			destinations = new ArrayList<String>();
			if(destA!=null){destinations.add(destA);}
			if(destB!=null){destinations.add(destB);}
		}
		this.currency = currency;
	}
	
	/**
	 * Constructor
	 * 
	 * @param id the identifier attributed (by the controller logic) to the flight search request
	 * @param dateFrom the beginning date of the time interval in which flights are of interest
	 * @param dateTo the end date of the time interval in which flights are of interest
	 * @param destinations the list of target flight destinations
	 * @param currency the currency in which the monetary values comprised in the results should be expressed 
	 */
	public Request(long id, String dateFrom, String dateTo, List<String> destinations, String currency)
	{
		this.id = id;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.destinations= destinations;
		this.currency = currency;
	}

	@JsonProperty("id")
	public long getId(){return id;}
	
	@JsonProperty("id")
	public void setId(long id){this.id=id;}
	
	@JsonProperty("dateFrom")
	public String getDateFrom() {return dateFrom;}
	
	@JsonProperty("dateFrom")
	public void setDateFrom(String dateFrom) {this.dateFrom=dateFrom;}
	
	@JsonProperty("dateTo")
	public String getDateTo() {return dateTo;}
	
	@JsonProperty("dateTo")
	public void setDateTo(String dateTo) {this.dateTo=dateTo;}
	
	@JsonProperty("destinations")
    public List<String> getDestinations() {return this.destinations;}
    
	@JsonProperty("destinations")
    public void setDestinations(List<String> destinations) {this.destinations = destinations;}

	@JsonProperty("currency")
	public String getCurrency(){return currency;}
	
	@JsonProperty("currency")
	public void setCurrency(String currency){this.currency=currency;}
}
