package cocus.flightinportugal;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import cocus.flightinportugal.datamodel.Request;
import cocus.flightinportugal.datamodel.RequestRepository;

public class DataPersistenceHandler
{
	RequestRepository mongoDBRepository;
	
	public DataPersistenceHandler(RequestRepository mongoDBRepository)
	{
		this.mongoDBRepository = mongoDBRepository;
	}

	/**
	 * Retrieves a specific Request object from the DB
	 * 
	 * @param id the id of the desired Request object
	 * @return an Optional object which may carry one or no Request objects
	 */
	public Optional<Request> getReqByID(long id)
	{
		synchronized(this){return mongoDBRepository.findById(String.valueOf(id));}
	}
	
	/**
	 * Returns all the flight search requests on record 
	 * 
	 * @return a list with a Request object for each of the flight search requests on record
	 */
	public List<Request> getAllSearchRequests()
	{
		synchronized(this){return mongoDBRepository.findAll();}
	}
	
	/**
	 * Stores the information describing a flight search request into a MongoDB database
	 * 
	 * @param reqID the unique identifier attributed to the request
	 * @param dateFrom the beginning date of the time interval for the search request in scope  
	 * @param dateTo the ending date of the time interval for the search request in scope
	 * @param destinations the target destination of the flights to employ in the flight search request
	 * @param currency the currency in which the values in scope should be presented back to the client side
	 */
	public void persistRequestInfo(long reqID, String dateFrom, String dateTo, List<String> destinations, String currency)
	{
		synchronized(this)
		{
			String destA = null;
			String destB = null;
			Iterator<String> destinationIterator = destinations.iterator();
			while(destinationIterator.hasNext())
			{
				String dest = destinationIterator.next();
				if(destA==null) {destA=dest;continue;}
				if(destB==null)
				{
					destB=dest;
					break;
				}
				else {break;}
			}
		
			//Build request object to be persisted
			Request request = new Request(reqID, dateFrom,dateTo,destA,destB,currency);
			persistRequest(request);
		}
	}

	public void persistRequests(List<Request> requests)
	{
		synchronized(this)
		{
			Iterator<Request> reqsIterator = requests.iterator();
			while(reqsIterator.hasNext())
			{
				Request req = reqsIterator.next();
				persistRequest(req);
			}
		}
	}
	
	private void persistRequest(Request request){mongoDBRepository.save(request);}
	
	public void deleteAllSearchRequests()
	{
		synchronized(this){mongoDBRepository.deleteAll();}
	}
}
