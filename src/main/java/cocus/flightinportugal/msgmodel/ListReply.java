package cocus.flightinportugal.msgmodel;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import cocus.flightinportugal.datamodel.Request;


/**
 * ListReply carries reply messages, from the FlightInPortugal service to the client side, in response to a request 
 * for the listing of previous flight search requests, as well as in response to a request for the elimination (at the FlightInPortugal service)
 * of all records of such requests.
 *  
 * The msg parameter consists of a message indicating the total number of available request records
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListReply
{	
	@JsonProperty("msg")
	private String msg;
	
	@JsonProperty("requests")
	private List<Request> requests;

	/**
	 * Constructor
	 * 
	 * @param msg the message indicating the total number o flight search requests recorded at the FlightInPortugal service
	 */
	public ListReply(String msg){this.msg=msg;}
	
	public ListReply(){}

	@JsonProperty("msg")
	public String getMsg(){return msg;}
	
	@JsonProperty("msg")
	public void setMsg(String msg){this.msg=msg;}
	
	@JsonProperty("requests")
	public List<Request> getRequests(){return requests;}
	
	@JsonProperty("requests")
	public void setRequests(List<Request> requests)
	{
		if(requests!=null && requests.size()>0){this.requests = requests;}
	}
}
