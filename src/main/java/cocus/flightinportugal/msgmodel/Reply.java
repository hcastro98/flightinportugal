package cocus.flightinportugal.msgmodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * A Reply object is the response object to a request to the FlightInPortugal averaging service
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"id",
	"error",
    "OPO",
    "LIS"
})
public class Reply
{
	@JsonProperty("id")
	private long id;
	
	@JsonProperty("error")
	private String errorMsg;
	
	@JsonProperty("OPO")
	private Destination OPO;
	
	@JsonProperty("LIS")
	private Destination LIS;

	/**
	 * @param id the identifier of the flight search request that lead to the creation of the reply object 
	 */
	public Reply(long id)
	{
		this.id=id;
	}

	/**
	 * @param id the identifier of the flight search request that lead to the creation of the reply object
	 * @param errorMsg the error message to be sent to the client side if some problem or error occurred or was detected within the 
	 * FlightInPortugal. If an error message is sent back from the Skypicker service it will be conveyed in this message
	 */
	public Reply(long id, String errorMsg)
	{
		this.id = id;
		this.errorMsg = errorMsg;
	}

	@JsonProperty("id")
	public long getId(){return id;}
	
	@JsonProperty("error")
	public String getErrorMsg(){return errorMsg;}
	
	@JsonProperty("LIS")
	public Destination getLIS(){return LIS;}
	
	/**
	 * Sets the reply values calculated from the information returned by Skypicker pertaining to flights destined to Lisbon (Oporto->Lisbon) 
	 * 
	 * @param avgFlightPrice the average flight price
	 * @param avgFirstBagPrice the average flight for transporting the first bag
	 * @param avgSecondBagPrice the average price for transporting the second bag
	 * @param dateFrom the beginning date of the time interval in which flights are of interest
	 * @param dateTo the end date of the time interval in which flights are of interest
	 * @param airport the name of the airport at the target destination city
	 * @param currency the currency in which the monetary values comprised in the results should be expressed 
	 */
	public void setLIS(Integer avgFlightPrice, Integer avgFirstBagPrice, Integer avgSecondBagPrice, String dateFrom,String dateTo, String airport, String currency)
	{
		LIS = new Destination(avgFlightPrice, avgFirstBagPrice, avgSecondBagPrice, dateFrom, dateTo, airport, currency);
	}
	
	public void setLIS(String errorMsg){LIS = new Destination(errorMsg);}

	@JsonProperty("OPO")
	public Destination getOPO() {return OPO;}

	/**
	 * Sets the reply values calculated from the information returned by Skypicker pertaining to flights destined to Oporto (Lisbon->Oporto) 
	 * 
	 * @param avgFlightPrice the average flight price
	 * @param avgFirstBagPrice the average flight for transporting the first bag
	 * @param avgSecondBagPrice the average price for transporting the second bag
	 * @param dateFrom the beginning date of the time interval in which flights are of interest
	 * @param dateTo the end date of the time interval in which flights are of interest
	 * @param airport the name of the airport at the target destination city
	 * @param currency the currency in which the monetary values comprised in the results are expressed 
	 */
	public void setOPO(Integer avgFlightPrice, Integer avgFirstBagPrice, Integer avgSecondBagPrice, String dateFrom,String dateTo, String airport, String currency)
	{
		OPO = new Destination(avgFlightPrice, avgFirstBagPrice, avgSecondBagPrice, dateFrom, dateTo, airport, currency);
	}
	
	public void setOPO(String errorMsg){OPO = new Destination(errorMsg);}

	/**
	 * A Destination class holds/groups the values (of the overall Reply object) that pertain to a specific flight destination
	 *
	 */
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonPropertyOrder({
		"airport",
	    "currency",
	    "avg_flight_price",
	    "date_from",
	    "date_to",
	    "bags_price",
	    "error"
	})
	public class Destination
	{
		@JsonProperty("airport")
		private String airport;
		
		@JsonProperty("currency")
		private String currency;
		
		@JsonProperty("avg_flight_price")
		private Integer avgFlightPrice;
		
		@JsonProperty("date_from")
		private String dateFrom;
		
		@JsonProperty("date_to")
		private String dateTo;
		
		@JsonProperty("bags_price")
		private BagsPrice bagsPrice;
		
		@JsonProperty("error")
		private String errorMsg;

		/**
		 * 
		 * @param avgFlightPrice the average flight price
		 * @param avgFirstBagPrice the average flight for transporting the first bag
		 * @param avgSecondBagPrice the average price for transporting the second bag
		 * @param dateFrom the beginning date of the time interval in which flights are of interest
		 * @param dateTo the end date of the time interval in which flights are of interest
		 * @param airport the name of the airport at the target destination city
		 * @param currency the currency in which the monetary values comprised in the results are expressed 
		 */
		public Destination(Integer avgFlightPrice, Integer avgFirstBagPrice, Integer avgSecondBagPrice, String dateFrom, String dateTo, String airport, String currency)
		{
			this.avgFlightPrice = avgFlightPrice;
			this.bagsPrice = new BagsPrice(avgFirstBagPrice, avgSecondBagPrice);
			this.dateFrom = dateFrom;
			this.dateTo = dateTo;
			this.airport = airport;
			this.currency = currency;
		}
		
		public Destination(String errorMsg){this.errorMsg = errorMsg;}
		
		@JsonProperty("airport")
		public String getAirport() {return airport;}
		
		@JsonProperty("currency")
		public String getCurrency() {return currency;}
		
		@JsonProperty("avg_flight_price")
		public Integer getAvgFlightPrice() {return avgFlightPrice;}

		@JsonProperty("date_from")
		public String getDateFrom() {return dateFrom;}

		@JsonProperty("date_to")
		public String getDateTo() {return dateTo;}
		
		@JsonProperty("bags_price")
		public BagsPrice getBagsPrice() {return bagsPrice;}
		
		@JsonProperty("error")
		public String getErrorMsg() {return errorMsg;}
		
		@JsonInclude(JsonInclude.Include.NON_NULL)
		@JsonPropertyOrder({
			"bag1_avgprice",
		    "bag2_avgprice"
		})
		class BagsPrice
		{
			@JsonProperty("bag1_avgprice")
			private Integer avgFirstBagPrice;
			
			@JsonProperty("bag2_avgprice")
			private Integer avgSecondBagPrice;
			
			BagsPrice(Integer avgFirstBagPrice, Integer avgSecondBagPrice)
			{
				this.avgFirstBagPrice = avgFirstBagPrice;
				this.avgSecondBagPrice = avgSecondBagPrice;
			}
			
			@JsonProperty("bag1_avgprice")
			public Integer getAvgFirstBagPrice() {return avgFirstBagPrice;}

			@JsonProperty("bag2_avgprice")
			public Integer getAvgSecondBagPrice() {return avgSecondBagPrice;}
		}	
	}
}
