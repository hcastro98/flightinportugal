package cocus.flightinportugal.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * MathUtils holds math related utility logic
 * 
 */
public class MathUtils
{

	/**
	 * Rounds the given double value and returns it as an Integer object
	 * 
	 * @param value the value to be rounded
	 * @return the rounded Integer value
	 */
	public static Integer round(double value)
	{
	    BigDecimal number = BigDecimal.valueOf(value);
	    number = number.setScale(0, RoundingMode.HALF_UP);
	    return number.intValue();
	}
}
