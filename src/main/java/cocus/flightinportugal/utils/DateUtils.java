package cocus.flightinportugal.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * This class aggregates all utility methods pertaining to manipulation of time and date values
 *
 */
public class DateUtils
{

	/**
	 * Builds a Calendar object corresponding to a time value expressed in milliseconds since UNIX epoch
	 * 
	 * @param timeInMillis time value expressed in milliseconds since UNIX epoch
	 * @return the Calendar object
	 */
	public static Calendar timeInMillis2Calendar(long timeInMillis)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(timeInMillis);
		return calendar;
	}

	/**
	 * Converts a time value expressed in milliseconds since Unix Epoch to dd/mm/yyy String format
	 * 
	 * @param timeInMillis the time value in milliseconds since UNIX epoch
	 * @return the time values expressed as a dd/mm/yyy String
	 */
	public static String timeInMillis2DateString(long timeInMillis)
	{
		String dateStr = "";
		Calendar calendar = timeInMillis2Calendar(timeInMillis);
		
		int mYear = calendar.get(Calendar.YEAR);
		int mMonth = calendar.get(Calendar.MONTH);
		int mDay = calendar.get(Calendar.DAY_OF_MONTH);
		
		dateStr = String.valueOf(mDay) + "/" + String.valueOf(mMonth) + "/" + String.valueOf(mYear);
		
		return dateStr;
	}

	/**
	 * Converts a date expressed in a dd/MM/yyyy String format to a value of "time in milliseconds since UNIX epoch"
	 * 
	 * @param dateStr the date String
	 * @return the time value expressed as "time in milliseconds since UNIX epoch"
	 * 
	 * @throws ParseException
	 */
	public static long dateStringToTimeInMillis(String dateStr) throws ParseException
	{
		//Date string format loike 2014/10/29
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = sdf.parse(dateStr);
		return date.getTime();
	}

}
