package cocus.flightinportugal.utils;

import java.util.List;

/**
 * Class providing the averaging functionalities
 * 
 * @param <T>
 */
public class Averager<T extends Number>
{
    private List<T> listOfQuantities;
    private Object  average;

    @SuppressWarnings("unchecked")
    /**
     * Constructor
     * 
     * @param list the list of values whose average is to be calculated
     */
    public Averager(List<T> list)
    {
        this.listOfQuantities = list;

        T sampleValue = listOfQuantities.get(0);
        if (sampleValue instanceof Integer)
        {
            calculateIntegerAverage((List<Integer>) listOfQuantities);
        }
        else if(sampleValue instanceof Double)
        {
            calculateDoubleAverage((List<Double>) listOfQuantities);
        } 
        else if (sampleValue instanceof Float)
        {
            calculateFloatAverage((List<Float>) listOfQuantities);
        }
        else
        {
            throw new IllegalStateException("The Averager's constructor must be initialized with a List<T> of Integer, Double or Float");
        }
    }

    /**
     * Calculates the average value of a given list of (Double) values
     * 
     * @param listOfQuantities the list of values
     */
    private void calculateDoubleAverage(List<Double> listOfQuantities)
    {
        Double total = Double.valueOf(0);
        for(Double amount : listOfQuantities){total += amount;}
        average = total/Double.valueOf(listOfQuantities.size());
    }

    /**
     * Calculates the average value of a given list of (Integer) values
     * 
     * @param listOfQuantities the list of values
     */
    private void calculateIntegerAverage(List<Integer> listOfQuantities)
    {
        Integer total = Integer.valueOf(0);
        for(Integer amount : listOfQuantities){total += amount;}
        average = total/Integer.valueOf(listOfQuantities.size());
    }

    /**
     * Calculates the average value of a given list of (Float) values
     * 
     * @param listOfQuantities the list of values
     */
    private void calculateFloatAverage(List<Float> listOfQuantities)
    {
        Float total = Float.valueOf(0);
        for(Float amount : listOfQuantities){total += amount;}
        average = total/Float.valueOf(listOfQuantities.size());
    }

    /**
     * Returns 
     * 
     * @return the calculated average value
     */
    public Object getAverage(){return average;}
}
