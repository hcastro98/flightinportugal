package cocus.flightinportugal.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * This class aggregates methods which are useful for some debugging aspects
 *
 */
public class DebugUtils
{

	/**
	 * Utility method which enables saving search results, received from Skypicker, into a specified local directory.
	 * It receives a HashMap<String,String> object whose keys are the flight destinations and whose values are Skypicker's replies to the 
	 * queries associated to those keys. It stores the search results of each query in a separate file.
	 * The files  will take name "normalised name of the destination city" + "_out.json". If a file with the same name exists it will be overwritten.
	 * 
	 * @param searchResults a HashMap<String,String> object whose keys are the flight destinations and whose values are Skypicker's replies to the 
	 * queries associated to those keys.
	 * @param outputDirPath the path to the directory where the files should be created
	 */
	public static void saveSearchResults(HashMap<String,Object> searchResults, String outputDirPath)
	{
	    Iterator<Entry<String,Object>> searchResultIterator = searchResults.entrySet().iterator();
	    while (searchResultIterator.hasNext())
	    {
	        Map.Entry<String,Object> pair = (Map.Entry<String,Object>) searchResultIterator.next();
	        String destName = pair.getKey();
	        Object searchResult = pair.getValue();
	       
		    ObjectMapper mapper = new ObjectMapper();
		    mapper.enable(SerializationFeature.INDENT_OUTPUT);
		    File outputJsonFile = new File(outputDirPath + destName + "_out.json");
	
			try
			{	
			    // Save JSON content to file
			    FileOutputStream fileOutputStream = new FileOutputStream(outputJsonFile);
			    mapper.writeValue(fileOutputStream, searchResult);
			    fileOutputStream.close();
			}
			catch (JsonProcessingException e)
			{
				e.printStackTrace();
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
	    }
	}

	/**
	 * Prints to console the contents of the given Iterable<S> object 
	 * 
	 * @param <S>
	 * @param <T>
	 * @param list the Iterable object
	 * @param msg the title message to be printed before the Iterable object's contents
	 */
	public <S, T extends Iterable<S>> void printIterableList(T list, String msg)
	{
		try
		{
	    	System.out.println(msg);
	        for (Object element : list){System.out.println(element.toString());}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage() + " while attempting to print the contents of an Iterable object");
		}
	
	}

}
