
package cocus.flightinportugal.skypicker.msgmodel;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "fare_basis",
    "fare_category",
    "fare_classes",
    "price",
    "fare_family",
    "found_on",
    "last_seen",
    "refresh_timestamp",
    "source",
    "return",
    "bags_recheck_required",
    "guarantee",
    "id",
    "combination_id",
    "original_return",
    "aTime",
    "dTime",
    "aTimeUTC",
    "dTimeUTC",
    "mapIdfrom",
    "mapIdto",
    "cityTo",
    "cityFrom",
    "cityCodeFrom",
    "cityCodeTo",
    "flyTo",
    "flyFrom",
    "airline",
    "operating_carrier",
    "equipment",
    "latFrom",
    "lngFrom",
    "latTo",
    "lngTo",
    "flight_no",
    "vehicle_type",
    "operating_flight_no"
})
public class Route {

    @JsonProperty("fare_basis")
    private String fareBasis;
    @JsonProperty("fare_category")
    private String fareCategory;
    @JsonProperty("fare_classes")
    private String fareClasses;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("fare_family")
    private String fareFamily;
    @JsonProperty("found_on")
    private String foundOn;
    @JsonProperty("last_seen")
    private Integer lastSeen;
    @JsonProperty("refresh_timestamp")
    private Integer refreshTimestamp;
    @JsonProperty("source")
    private String source;
    @JsonProperty("return")
    private Integer _return;
    @JsonProperty("bags_recheck_required")
    private Boolean bagsRecheckRequired;
    @JsonProperty("guarantee")
    private Boolean guarantee;
    @JsonProperty("id")
    private String id;
    @JsonProperty("combination_id")
    private String combinationId;
    @JsonProperty("original_return")
    private Integer originalReturn;
    @JsonProperty("aTime")
    private Integer aTime;
    @JsonProperty("dTime")
    private Integer dTime;
    @JsonProperty("aTimeUTC")
    private Integer aTimeUTC;
    @JsonProperty("dTimeUTC")
    private Integer dTimeUTC;
    @JsonProperty("mapIdfrom")
    private String mapIdfrom;
    @JsonProperty("mapIdto")
    private String mapIdto;
    @JsonProperty("cityTo")
    private String cityTo;
    @JsonProperty("cityFrom")
    private String cityFrom;
    @JsonProperty("cityCodeFrom")
    private String cityCodeFrom;
    @JsonProperty("cityCodeTo")
    private String cityCodeTo;
    @JsonProperty("flyTo")
    private String flyTo;
    @JsonProperty("flyFrom")
    private String flyFrom;
    @JsonProperty("airline")
    private String airline;
    @JsonProperty("operating_carrier")
    private String operatingCarrier;
    @JsonProperty("equipment")
    private Object equipment;
    @JsonProperty("latFrom")
    private Double latFrom;
    @JsonProperty("lngFrom")
    private Double lngFrom;
    @JsonProperty("latTo")
    private Double latTo;
    @JsonProperty("lngTo")
    private Double lngTo;
    @JsonProperty("flight_no")
    private Integer flightNo;
    @JsonProperty("vehicle_type")
    private String vehicleType;
    @JsonProperty("operating_flight_no")
    private String operatingFlightNo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Route() {
    }

    /**
     * 
     * @param latFrom
     * @param fareClasses
     * @param foundOn
     * @param mapIdto
     * @param guarantee
     * @param cityFrom
     * @param source
     * @param fareCategory
     * @param lngFrom
     * @param dTimeUTC
     * @param aTimeUTC
     * @param price
     * @param refreshTimestamp
     * @param id
     * @param airline
     * @param operatingCarrier
     * @param flyTo
     * @param vehicleType
     * @param operatingFlightNo
     * @param cityCodeTo
     * @param latTo
     * @param lngTo
     * @param combinationId
     * @param equipment
     * @param cityTo
     * @param bagsRecheckRequired
     * @param flyFrom
     * @param originalReturn
     * @param aTime
     * @param _return
     * @param lastSeen
     * @param mapIdfrom
     * @param flightNo
     * @param fareBasis
     * @param cityCodeFrom
     * @param dTime
     * @param fareFamily
     */
    public Route(String fareBasis, String fareCategory, String fareClasses, Integer price, String fareFamily, String foundOn, Integer lastSeen, Integer refreshTimestamp, String source, Integer _return, Boolean bagsRecheckRequired, Boolean guarantee, String id, String combinationId, Integer originalReturn, Integer aTime, Integer dTime, Integer aTimeUTC, Integer dTimeUTC, String mapIdfrom, String mapIdto, String cityTo, String cityFrom, String cityCodeFrom, String cityCodeTo, String flyTo, String flyFrom, String airline, String operatingCarrier, Object equipment, Double latFrom, Double lngFrom, Double latTo, Double lngTo, Integer flightNo, String vehicleType, String operatingFlightNo) {
        super();
        this.fareBasis = fareBasis;
        this.fareCategory = fareCategory;
        this.fareClasses = fareClasses;
        this.price = price;
        this.fareFamily = fareFamily;
        this.foundOn = foundOn;
        this.lastSeen = lastSeen;
        this.refreshTimestamp = refreshTimestamp;
        this.source = source;
        this._return = _return;
        this.bagsRecheckRequired = bagsRecheckRequired;
        this.guarantee = guarantee;
        this.id = id;
        this.combinationId = combinationId;
        this.originalReturn = originalReturn;
        this.aTime = aTime;
        this.dTime = dTime;
        this.aTimeUTC = aTimeUTC;
        this.dTimeUTC = dTimeUTC;
        this.mapIdfrom = mapIdfrom;
        this.mapIdto = mapIdto;
        this.cityTo = cityTo;
        this.cityFrom = cityFrom;
        this.cityCodeFrom = cityCodeFrom;
        this.cityCodeTo = cityCodeTo;
        this.flyTo = flyTo;
        this.flyFrom = flyFrom;
        this.airline = airline;
        this.operatingCarrier = operatingCarrier;
        this.equipment = equipment;
        this.latFrom = latFrom;
        this.lngFrom = lngFrom;
        this.latTo = latTo;
        this.lngTo = lngTo;
        this.flightNo = flightNo;
        this.vehicleType = vehicleType;
        this.operatingFlightNo = operatingFlightNo;
    }

    @JsonProperty("fare_basis")
    public String getFareBasis() {
        return fareBasis;
    }

    @JsonProperty("fare_basis")
    public void setFareBasis(String fareBasis) {
        this.fareBasis = fareBasis;
    }

    @JsonProperty("fare_category")
    public String getFareCategory() {
        return fareCategory;
    }

    @JsonProperty("fare_category")
    public void setFareCategory(String fareCategory) {
        this.fareCategory = fareCategory;
    }

    @JsonProperty("fare_classes")
    public String getFareClasses() {
        return fareClasses;
    }

    @JsonProperty("fare_classes")
    public void setFareClasses(String fareClasses) {
        this.fareClasses = fareClasses;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("fare_family")
    public String getFareFamily() {
        return fareFamily;
    }

    @JsonProperty("fare_family")
    public void setFareFamily(String fareFamily) {
        this.fareFamily = fareFamily;
    }

    @JsonProperty("found_on")
    public String getFoundOn() {
        return foundOn;
    }

    @JsonProperty("found_on")
    public void setFoundOn(String foundOn) {
        this.foundOn = foundOn;
    }

    @JsonProperty("last_seen")
    public Integer getLastSeen() {
        return lastSeen;
    }

    @JsonProperty("last_seen")
    public void setLastSeen(Integer lastSeen) {
        this.lastSeen = lastSeen;
    }

    @JsonProperty("refresh_timestamp")
    public Integer getRefreshTimestamp() {
        return refreshTimestamp;
    }

    @JsonProperty("refresh_timestamp")
    public void setRefreshTimestamp(Integer refreshTimestamp) {
        this.refreshTimestamp = refreshTimestamp;
    }

    @JsonProperty("source")
    public String getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(String source) {
        this.source = source;
    }

    @JsonProperty("return")
    public Integer getReturn() {
        return _return;
    }

    @JsonProperty("return")
    public void setReturn(Integer _return) {
        this._return = _return;
    }

    @JsonProperty("bags_recheck_required")
    public Boolean getBagsRecheckRequired() {
        return bagsRecheckRequired;
    }

    @JsonProperty("bags_recheck_required")
    public void setBagsRecheckRequired(Boolean bagsRecheckRequired) {
        this.bagsRecheckRequired = bagsRecheckRequired;
    }

    @JsonProperty("guarantee")
    public Boolean getGuarantee() {
        return guarantee;
    }

    @JsonProperty("guarantee")
    public void setGuarantee(Boolean guarantee) {
        this.guarantee = guarantee;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("combination_id")
    public String getCombinationId() {
        return combinationId;
    }

    @JsonProperty("combination_id")
    public void setCombinationId(String combinationId) {
        this.combinationId = combinationId;
    }

    @JsonProperty("original_return")
    public Integer getOriginalReturn() {
        return originalReturn;
    }

    @JsonProperty("original_return")
    public void setOriginalReturn(Integer originalReturn) {
        this.originalReturn = originalReturn;
    }

    @JsonProperty("aTime")
    public Integer getATime() {
        return aTime;
    }

    @JsonProperty("aTime")
    public void setATime(Integer aTime) {
        this.aTime = aTime;
    }

    @JsonProperty("dTime")
    public Integer getDTime() {
        return dTime;
    }

    @JsonProperty("dTime")
    public void setDTime(Integer dTime) {
        this.dTime = dTime;
    }

    @JsonProperty("aTimeUTC")
    public Integer getATimeUTC() {
        return aTimeUTC;
    }

    @JsonProperty("aTimeUTC")
    public void setATimeUTC(Integer aTimeUTC) {
        this.aTimeUTC = aTimeUTC;
    }

    @JsonProperty("dTimeUTC")
    public Integer getDTimeUTC() {
        return dTimeUTC;
    }

    @JsonProperty("dTimeUTC")
    public void setDTimeUTC(Integer dTimeUTC) {
        this.dTimeUTC = dTimeUTC;
    }

    @JsonProperty("mapIdfrom")
    public String getMapIdfrom() {
        return mapIdfrom;
    }

    @JsonProperty("mapIdfrom")
    public void setMapIdfrom(String mapIdfrom) {
        this.mapIdfrom = mapIdfrom;
    }

    @JsonProperty("mapIdto")
    public String getMapIdto() {
        return mapIdto;
    }

    @JsonProperty("mapIdto")
    public void setMapIdto(String mapIdto) {
        this.mapIdto = mapIdto;
    }

    @JsonProperty("cityTo")
    public String getCityTo() {
        return cityTo;
    }

    @JsonProperty("cityTo")
    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    @JsonProperty("cityFrom")
    public String getCityFrom() {
        return cityFrom;
    }

    @JsonProperty("cityFrom")
    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    @JsonProperty("cityCodeFrom")
    public String getCityCodeFrom() {
        return cityCodeFrom;
    }

    @JsonProperty("cityCodeFrom")
    public void setCityCodeFrom(String cityCodeFrom) {
        this.cityCodeFrom = cityCodeFrom;
    }

    @JsonProperty("cityCodeTo")
    public String getCityCodeTo() {
        return cityCodeTo;
    }

    @JsonProperty("cityCodeTo")
    public void setCityCodeTo(String cityCodeTo) {
        this.cityCodeTo = cityCodeTo;
    }

    @JsonProperty("flyTo")
    public String getFlyTo() {
        return flyTo;
    }

    @JsonProperty("flyTo")
    public void setFlyTo(String flyTo) {
        this.flyTo = flyTo;
    }

    @JsonProperty("flyFrom")
    public String getFlyFrom() {
        return flyFrom;
    }

    @JsonProperty("flyFrom")
    public void setFlyFrom(String flyFrom) {
        this.flyFrom = flyFrom;
    }

    @JsonProperty("airline")
    public String getAirline() {
        return airline;
    }

    @JsonProperty("airline")
    public void setAirline(String airline) {
        this.airline = airline;
    }

    @JsonProperty("operating_carrier")
    public String getOperatingCarrier() {
        return operatingCarrier;
    }

    @JsonProperty("operating_carrier")
    public void setOperatingCarrier(String operatingCarrier) {
        this.operatingCarrier = operatingCarrier;
    }

    @JsonProperty("equipment")
    public Object getEquipment() {
        return equipment;
    }

    @JsonProperty("equipment")
    public void setEquipment(Object equipment) {
        this.equipment = equipment;
    }

    @JsonProperty("latFrom")
    public Double getLatFrom() {
        return latFrom;
    }

    @JsonProperty("latFrom")
    public void setLatFrom(Double latFrom) {
        this.latFrom = latFrom;
    }

    @JsonProperty("lngFrom")
    public Double getLngFrom() {
        return lngFrom;
    }

    @JsonProperty("lngFrom")
    public void setLngFrom(Double lngFrom) {
        this.lngFrom = lngFrom;
    }

    @JsonProperty("latTo")
    public Double getLatTo() {
        return latTo;
    }

    @JsonProperty("latTo")
    public void setLatTo(Double latTo) {
        this.latTo = latTo;
    }

    @JsonProperty("lngTo")
    public Double getLngTo() {
        return lngTo;
    }

    @JsonProperty("lngTo")
    public void setLngTo(Double lngTo) {
        this.lngTo = lngTo;
    }

    @JsonProperty("flight_no")
    public Integer getFlightNo() {
        return flightNo;
    }

    @JsonProperty("flight_no")
    public void setFlightNo(Integer flightNo) {
        this.flightNo = flightNo;
    }

    @JsonProperty("vehicle_type")
    public String getVehicleType() {
        return vehicleType;
    }

    @JsonProperty("vehicle_type")
    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    @JsonProperty("operating_flight_no")
    public String getOperatingFlightNo() {
        return operatingFlightNo;
    }

    @JsonProperty("operating_flight_no")
    public void setOperatingFlightNo(String operatingFlightNo) {
        this.operatingFlightNo = operatingFlightNo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("fareBasis", fareBasis).append("fareCategory", fareCategory).append("fareClasses", fareClasses).append("price", price).append("fareFamily", fareFamily).append("foundOn", foundOn).append("lastSeen", lastSeen).append("refreshTimestamp", refreshTimestamp).append("source", source).append("_return", _return).append("bagsRecheckRequired", bagsRecheckRequired).append("guarantee", guarantee).append("id", id).append("combinationId", combinationId).append("originalReturn", originalReturn).append("aTime", aTime).append("dTime", dTime).append("aTimeUTC", aTimeUTC).append("dTimeUTC", dTimeUTC).append("mapIdfrom", mapIdfrom).append("mapIdto", mapIdto).append("cityTo", cityTo).append("cityFrom", cityFrom).append("cityCodeFrom", cityCodeFrom).append("cityCodeTo", cityCodeTo).append("flyTo", flyTo).append("flyFrom", flyFrom).append("airline", airline).append("operatingCarrier", operatingCarrier).append("equipment", equipment).append("latFrom", latFrom).append("lngFrom", lngFrom).append("latTo", latTo).append("lngTo", lngTo).append("flightNo", flightNo).append("vehicleType", vehicleType).append("operatingFlightNo", operatingFlightNo).append("additionalProperties", additionalProperties).toString();
    }

}
