
package cocus.flightinportugal.skypicker.msgmodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * A Search object holds a response sent back from the Skypicker service
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "search_id",
    "data",
    "connections",
    "time",
    "currency",
    "currency_rate",
    "fx_rate",
    "refresh",
    "del",
    "ref_tasks",
    "search_params",
    "all_stopover_airports",
    "all_airlines"
})
public class Search {

    @JsonProperty("search_id")
    private String searchId;
    @JsonProperty("data")
    private List<Datum> data = null;
    @JsonProperty("connections")
    private List<Object> connections = null;
    @JsonProperty("time")
    private Integer time;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("currency_rate")
    private Double currencyRate;
    @JsonProperty("fx_rate")
    private Double fxRate;
    @JsonProperty("refresh")
    private List<Object> refresh = null;
    @JsonProperty("del")
    private Double del;
    @JsonProperty("ref_tasks")
    private List<Object> refTasks = null;
    @JsonProperty("search_params")
    private SearchParams searchParams;
    @JsonProperty("all_stopover_airports")
    private List<Object> allStopoverAirports = null;
    @JsonProperty("all_airlines")
    private List<Object> allAirlines = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Search() {
    }

    /**
     * 
     * @param allStopoverAirports
     * @param fxRate
     * @param data
     * @param refresh
     * @param del
     * @param refTasks
     * @param searchParams
     * @param searchId
     * @param currency
     * @param time
     * @param currencyRate
     * @param connections
     * @param allAirlines
     */
    public Search(String searchId, List<Datum> data, List<Object> connections, Integer time, String currency, Double currencyRate, Double fxRate, List<Object> refresh, Double del, List<Object> refTasks, SearchParams searchParams, List<Object> allStopoverAirports, List<Object> allAirlines) {
        super();
        this.searchId = searchId;
        this.data = data;
        this.connections = connections;
        this.time = time;
        this.currency = currency;
        this.currencyRate = currencyRate;
        this.fxRate = fxRate;
        this.refresh = refresh;
        this.del = del;
        this.refTasks = refTasks;
        this.searchParams = searchParams;
        this.allStopoverAirports = allStopoverAirports;
        this.allAirlines = allAirlines;
    }

    @JsonProperty("search_id")
    public String getSearchId() {
        return searchId;
    }

    @JsonProperty("search_id")
    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    @JsonProperty("data")
    public List<Datum> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<Datum> data) {
        this.data = data;
    }

    @JsonProperty("connections")
    public List<Object> getConnections() {
        return connections;
    }

    @JsonProperty("connections")
    public void setConnections(List<Object> connections) {
        this.connections = connections;
    }

    @JsonProperty("time")
    public Integer getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(Integer time) {
        this.time = time;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("currency_rate")
    public Double getCurrencyRate() {
        return currencyRate;
    }

    @JsonProperty("currency_rate")
    public void setCurrencyRate(Double currencyRate) {
        this.currencyRate = currencyRate;
    }

    @JsonProperty("fx_rate")
    public Double getFxRate() {
        return fxRate;
    }

    @JsonProperty("fx_rate")
    public void setFxRate(Double fxRate) {
        this.fxRate = fxRate;
    }

    @JsonProperty("refresh")
    public List<Object> getRefresh() {
        return refresh;
    }

    @JsonProperty("refresh")
    public void setRefresh(List<Object> refresh) {
        this.refresh = refresh;
    }

    @JsonProperty("del")
    public Double getDel() {
        return del;
    }

    @JsonProperty("del")
    public void setDel(Double del) {
        this.del = del;
    }

    @JsonProperty("ref_tasks")
    public List<Object> getRefTasks() {
        return refTasks;
    }

    @JsonProperty("ref_tasks")
    public void setRefTasks(List<Object> refTasks) {
        this.refTasks = refTasks;
    }

    @JsonProperty("search_params")
    public SearchParams getSearchParams() {
        return searchParams;
    }

    @JsonProperty("search_params")
    public void setSearchParams(SearchParams searchParams) {
        this.searchParams = searchParams;
    }

    @JsonProperty("all_stopover_airports")
    public List<Object> getAllStopoverAirports() {
        return allStopoverAirports;
    }

    @JsonProperty("all_stopover_airports")
    public void setAllStopoverAirports(List<Object> allStopoverAirports) {
        this.allStopoverAirports = allStopoverAirports;
    }

    @JsonProperty("all_airlines")
    public List<Object> getAllAirlines() {
        return allAirlines;
    }

    @JsonProperty("all_airlines")
    public void setAllAirlines(List<Object> allAirlines) {
        this.allAirlines = allAirlines;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("searchId", searchId).append("data", data).append("connections", connections).append("time", time).append("currency", currency).append("currencyRate", currencyRate).append("fxRate", fxRate).append("refresh", refresh).append("del", del).append("refTasks", refTasks).append("searchParams", searchParams).append("allStopoverAirports", allStopoverAirports).append("allAirlines", allAirlines).append("additionalProperties", additionalProperties).toString();
    }

}
