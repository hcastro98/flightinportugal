
package cocus.flightinportugal.skypicker.msgmodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "dTime",
    "dTimeUTC",
    "aTime",
    "aTimeUTC",
    "nightsInDest",
    "duration",
    "fly_duration",
    "flyFrom",
    "cityFrom",
    "cityCodeFrom",
    "countryFrom",
    "mapIdfrom",
    "flyTo",
    "cityTo",
    "cityCodeTo",
    "countryTo",
    "mapIdto",
    "distance",
    "routes",
    "airlines",
    "pnr_count",
    "has_airport_change",
    "technical_stops",
    "price",
    "bags_price",
    "baglimit",
    "availability",
    "facilitated_booking_available",
    "conversion",
    "quality",
    "booking_token",
    "deep_link",
    "tracking_pixel",
    "p1",
    "p2",
    "p3",
    "transfers",
    "type_flights",
    "virtual_interlining",
    "found_on",
    "route"
})
public class Datum {

    @JsonProperty("id")
    private String id;
    @JsonProperty("dTime")
    private Integer dTime;
    @JsonProperty("dTimeUTC")
    private Integer dTimeUTC;
    @JsonProperty("aTime")
    private Integer aTime;
    @JsonProperty("aTimeUTC")
    private Integer aTimeUTC;
    @JsonProperty("nightsInDest")
    private Object nightsInDest;
    @JsonProperty("duration")
    private Duration duration;
    @JsonProperty("fly_duration")
    private String flyDuration;
    @JsonProperty("flyFrom")
    private String flyFrom;
    @JsonProperty("cityFrom")
    private String cityFrom;
    @JsonProperty("cityCodeFrom")
    private String cityCodeFrom;
    @JsonProperty("countryFrom")
    private CountryFrom countryFrom;
    @JsonProperty("mapIdfrom")
    private String mapIdfrom;
    @JsonProperty("flyTo")
    private String flyTo;
    @JsonProperty("cityTo")
    private String cityTo;
    @JsonProperty("cityCodeTo")
    private String cityCodeTo;
    @JsonProperty("countryTo")
    private CountryTo countryTo;
    @JsonProperty("mapIdto")
    private String mapIdto;
    @JsonProperty("distance")
    private Double distance;
    @JsonProperty("routes")
    private List<List<String>> routes = null;
    @JsonProperty("airlines")
    private List<String> airlines = null;
    @JsonProperty("pnr_count")
    private Integer pnrCount;
    @JsonProperty("has_airport_change")
    private Boolean hasAirportChange;
    @JsonProperty("technical_stops")
    private Integer technicalStops;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("bags_price")
    private BagsPrice bagsPrice;
    @JsonProperty("baglimit")
    private Baglimit baglimit;
    @JsonProperty("availability")
    private Availability availability;
    @JsonProperty("facilitated_booking_available")
    private Boolean facilitatedBookingAvailable;
    @JsonProperty("conversion")
    private Conversion conversion;
    @JsonProperty("quality")
    private Double quality;
    @JsonProperty("booking_token")
    private String bookingToken;
    @JsonProperty("deep_link")
    private String deepLink;
    @JsonProperty("tracking_pixel")
    private Object trackingPixel;
    @JsonProperty("p1")
    private Integer p1;
    @JsonProperty("p2")
    private Integer p2;
    @JsonProperty("p3")
    private Integer p3;
    @JsonProperty("transfers")
    private List<Object> transfers = null;
    @JsonProperty("type_flights")
    private List<String> typeFlights = null;
    @JsonProperty("virtual_interlining")
    private Boolean virtualInterlining;
    @JsonProperty("found_on")
    private List<String> foundOn = null;
    @JsonProperty("route")
    private List<Route> route = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Datum() {
    }

    /**
     * 
     * @param p1
     * @param p2
     * @param hasAirportChange
     * @param p3
     * @param distance
     * @param technicalStops
     * @param trackingPixel
     * @param mapIdto
     * @param foundOn
     * @param cityFrom
     * @param availability
     * @param dTimeUTC
     * @param aTimeUTC
     * @param duration
     * @param routes
     * @param nightsInDest
     * @param price
     * @param transfers
     * @param countryFrom
     * @param airlines
     * @param id
     * @param bookingToken
     * @param flyTo
     * @param bagsPrice
     * @param conversion
     * @param cityCodeTo
     * @param baglimit
     * @param pnrCount
     * @param virtualInterlining
     * @param cityTo
     * @param flyFrom
     * @param countryTo
     * @param quality
     * @param aTime
     * @param facilitatedBookingAvailable
     * @param mapIdfrom
     * @param route
     * @param deepLink
     * @param flyDuration
     * @param cityCodeFrom
     * @param dTime
     * @param typeFlights
     */
    public Datum(String id, Integer dTime, Integer dTimeUTC, Integer aTime, Integer aTimeUTC, Object nightsInDest, Duration duration, String flyDuration, String flyFrom, String cityFrom, String cityCodeFrom, CountryFrom countryFrom, String mapIdfrom, String flyTo, String cityTo, String cityCodeTo, CountryTo countryTo, String mapIdto, Double distance, List<List<String>> routes, List<String> airlines, Integer pnrCount, Boolean hasAirportChange, Integer technicalStops, Integer price, BagsPrice bagsPrice, Baglimit baglimit, Availability availability, Boolean facilitatedBookingAvailable, Conversion conversion, Double quality, String bookingToken, String deepLink, Object trackingPixel, Integer p1, Integer p2, Integer p3, List<Object> transfers, List<String> typeFlights, Boolean virtualInterlining, List<String> foundOn, List<Route> route) {
        super();
        this.id = id;
        this.dTime = dTime;
        this.dTimeUTC = dTimeUTC;
        this.aTime = aTime;
        this.aTimeUTC = aTimeUTC;
        this.nightsInDest = nightsInDest;
        this.duration = duration;
        this.flyDuration = flyDuration;
        this.flyFrom = flyFrom;
        this.cityFrom = cityFrom;
        this.cityCodeFrom = cityCodeFrom;
        this.countryFrom = countryFrom;
        this.mapIdfrom = mapIdfrom;
        this.flyTo = flyTo;
        this.cityTo = cityTo;
        this.cityCodeTo = cityCodeTo;
        this.countryTo = countryTo;
        this.mapIdto = mapIdto;
        this.distance = distance;
        this.routes = routes;
        this.airlines = airlines;
        this.pnrCount = pnrCount;
        this.hasAirportChange = hasAirportChange;
        this.technicalStops = technicalStops;
        this.price = price;
        this.bagsPrice = bagsPrice;
        this.baglimit = baglimit;
        this.availability = availability;
        this.facilitatedBookingAvailable = facilitatedBookingAvailable;
        this.conversion = conversion;
        this.quality = quality;
        this.bookingToken = bookingToken;
        this.deepLink = deepLink;
        this.trackingPixel = trackingPixel;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.transfers = transfers;
        this.typeFlights = typeFlights;
        this.virtualInterlining = virtualInterlining;
        this.foundOn = foundOn;
        this.route = route;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("dTime")
    public Integer getDTime() {
        return dTime;
    }

    @JsonProperty("dTime")
    public void setDTime(Integer dTime) {
        this.dTime = dTime;
    }

    @JsonProperty("dTimeUTC")
    public Integer getDTimeUTC() {
        return dTimeUTC;
    }

    @JsonProperty("dTimeUTC")
    public void setDTimeUTC(Integer dTimeUTC) {
        this.dTimeUTC = dTimeUTC;
    }

    @JsonProperty("aTime")
    public Integer getATime() {
        return aTime;
    }

    @JsonProperty("aTime")
    public void setATime(Integer aTime) {
        this.aTime = aTime;
    }

    @JsonProperty("aTimeUTC")
    public Integer getATimeUTC() {
        return aTimeUTC;
    }

    @JsonProperty("aTimeUTC")
    public void setATimeUTC(Integer aTimeUTC) {
        this.aTimeUTC = aTimeUTC;
    }

    @JsonProperty("nightsInDest")
    public Object getNightsInDest() {
        return nightsInDest;
    }

    @JsonProperty("nightsInDest")
    public void setNightsInDest(Object nightsInDest) {
        this.nightsInDest = nightsInDest;
    }

    @JsonProperty("duration")
    public Duration getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    @JsonProperty("fly_duration")
    public String getFlyDuration() {
        return flyDuration;
    }

    @JsonProperty("fly_duration")
    public void setFlyDuration(String flyDuration) {
        this.flyDuration = flyDuration;
    }

    @JsonProperty("flyFrom")
    public String getFlyFrom() {
        return flyFrom;
    }

    @JsonProperty("flyFrom")
    public void setFlyFrom(String flyFrom) {
        this.flyFrom = flyFrom;
    }

    @JsonProperty("cityFrom")
    public String getCityFrom() {
        return cityFrom;
    }

    @JsonProperty("cityFrom")
    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    @JsonProperty("cityCodeFrom")
    public String getCityCodeFrom() {
        return cityCodeFrom;
    }

    @JsonProperty("cityCodeFrom")
    public void setCityCodeFrom(String cityCodeFrom) {
        this.cityCodeFrom = cityCodeFrom;
    }

    @JsonProperty("countryFrom")
    public CountryFrom getCountryFrom() {
        return countryFrom;
    }

    @JsonProperty("countryFrom")
    public void setCountryFrom(CountryFrom countryFrom) {
        this.countryFrom = countryFrom;
    }

    @JsonProperty("mapIdfrom")
    public String getMapIdfrom() {
        return mapIdfrom;
    }

    @JsonProperty("mapIdfrom")
    public void setMapIdfrom(String mapIdfrom) {
        this.mapIdfrom = mapIdfrom;
    }

    @JsonProperty("flyTo")
    public String getFlyTo() {
        return flyTo;
    }

    @JsonProperty("flyTo")
    public void setFlyTo(String flyTo) {
        this.flyTo = flyTo;
    }

    @JsonProperty("cityTo")
    public String getCityTo() {
        return cityTo;
    }

    @JsonProperty("cityTo")
    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    @JsonProperty("cityCodeTo")
    public String getCityCodeTo() {
        return cityCodeTo;
    }

    @JsonProperty("cityCodeTo")
    public void setCityCodeTo(String cityCodeTo) {
        this.cityCodeTo = cityCodeTo;
    }

    @JsonProperty("countryTo")
    public CountryTo getCountryTo() {
        return countryTo;
    }

    @JsonProperty("countryTo")
    public void setCountryTo(CountryTo countryTo) {
        this.countryTo = countryTo;
    }

    @JsonProperty("mapIdto")
    public String getMapIdto() {
        return mapIdto;
    }

    @JsonProperty("mapIdto")
    public void setMapIdto(String mapIdto) {
        this.mapIdto = mapIdto;
    }

    @JsonProperty("distance")
    public Double getDistance() {
        return distance;
    }

    @JsonProperty("distance")
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @JsonProperty("routes")
    public List<List<String>> getRoutes() {
        return routes;
    }

    @JsonProperty("routes")
    public void setRoutes(List<List<String>> routes) {
        this.routes = routes;
    }

    @JsonProperty("airlines")
    public List<String> getAirlines() {
        return airlines;
    }

    @JsonProperty("airlines")
    public void setAirlines(List<String> airlines) {
        this.airlines = airlines;
    }

    @JsonProperty("pnr_count")
    public Integer getPnrCount() {
        return pnrCount;
    }

    @JsonProperty("pnr_count")
    public void setPnrCount(Integer pnrCount) {
        this.pnrCount = pnrCount;
    }

    @JsonProperty("has_airport_change")
    public Boolean getHasAirportChange() {
        return hasAirportChange;
    }

    @JsonProperty("has_airport_change")
    public void setHasAirportChange(Boolean hasAirportChange) {
        this.hasAirportChange = hasAirportChange;
    }

    @JsonProperty("technical_stops")
    public Integer getTechnicalStops() {
        return technicalStops;
    }

    @JsonProperty("technical_stops")
    public void setTechnicalStops(Integer technicalStops) {
        this.technicalStops = technicalStops;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("bags_price")
    public BagsPrice getBagsPrice() {
        return bagsPrice;
    }

    @JsonProperty("bags_price")
    public void setBagsPrice(BagsPrice bagsPrice) {
        this.bagsPrice = bagsPrice;
    }

    @JsonProperty("baglimit")
    public Baglimit getBaglimit() {
        return baglimit;
    }

    @JsonProperty("baglimit")
    public void setBaglimit(Baglimit baglimit) {
        this.baglimit = baglimit;
    }

    @JsonProperty("availability")
    public Availability getAvailability() {
        return availability;
    }

    @JsonProperty("availability")
    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    @JsonProperty("facilitated_booking_available")
    public Boolean getFacilitatedBookingAvailable() {
        return facilitatedBookingAvailable;
    }

    @JsonProperty("facilitated_booking_available")
    public void setFacilitatedBookingAvailable(Boolean facilitatedBookingAvailable) {
        this.facilitatedBookingAvailable = facilitatedBookingAvailable;
    }

    @JsonProperty("conversion")
    public Conversion getConversion() {
        return conversion;
    }

    @JsonProperty("conversion")
    public void setConversion(Conversion conversion) {
        this.conversion = conversion;
    }

    @JsonProperty("quality")
    public Double getQuality() {
        return quality;
    }

    @JsonProperty("quality")
    public void setQuality(Double quality) {
        this.quality = quality;
    }

    @JsonProperty("booking_token")
    public String getBookingToken() {
        return bookingToken;
    }

    @JsonProperty("booking_token")
    public void setBookingToken(String bookingToken) {
        this.bookingToken = bookingToken;
    }

    @JsonProperty("deep_link")
    public String getDeepLink() {
        return deepLink;
    }

    @JsonProperty("deep_link")
    public void setDeepLink(String deepLink) {
        this.deepLink = deepLink;
    }

    @JsonProperty("tracking_pixel")
    public Object getTrackingPixel() {
        return trackingPixel;
    }

    @JsonProperty("tracking_pixel")
    public void setTrackingPixel(Object trackingPixel) {
        this.trackingPixel = trackingPixel;
    }

    @JsonProperty("p1")
    public Integer getP1() {
        return p1;
    }

    @JsonProperty("p1")
    public void setP1(Integer p1) {
        this.p1 = p1;
    }

    @JsonProperty("p2")
    public Integer getP2() {
        return p2;
    }

    @JsonProperty("p2")
    public void setP2(Integer p2) {
        this.p2 = p2;
    }

    @JsonProperty("p3")
    public Integer getP3() {
        return p3;
    }

    @JsonProperty("p3")
    public void setP3(Integer p3) {
        this.p3 = p3;
    }

    @JsonProperty("transfers")
    public List<Object> getTransfers() {
        return transfers;
    }

    @JsonProperty("transfers")
    public void setTransfers(List<Object> transfers) {
        this.transfers = transfers;
    }

    @JsonProperty("type_flights")
    public List<String> getTypeFlights() {
        return typeFlights;
    }

    @JsonProperty("type_flights")
    public void setTypeFlights(List<String> typeFlights) {
        this.typeFlights = typeFlights;
    }

    @JsonProperty("virtual_interlining")
    public Boolean getVirtualInterlining() {
        return virtualInterlining;
    }

    @JsonProperty("virtual_interlining")
    public void setVirtualInterlining(Boolean virtualInterlining) {
        this.virtualInterlining = virtualInterlining;
    }

    @JsonProperty("found_on")
    public List<String> getFoundOn() {
        return foundOn;
    }

    @JsonProperty("found_on")
    public void setFoundOn(List<String> foundOn) {
        this.foundOn = foundOn;
    }

    @JsonProperty("route")
    public List<Route> getRoute() {
        return route;
    }

    @JsonProperty("route")
    public void setRoute(List<Route> route) {
        this.route = route;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("dTime", dTime).append("dTimeUTC", dTimeUTC).append("aTime", aTime).append("aTimeUTC", aTimeUTC).append("nightsInDest", nightsInDest).append("duration", duration).append("flyDuration", flyDuration).append("flyFrom", flyFrom).append("cityFrom", cityFrom).append("cityCodeFrom", cityCodeFrom).append("countryFrom", countryFrom).append("mapIdfrom", mapIdfrom).append("flyTo", flyTo).append("cityTo", cityTo).append("cityCodeTo", cityCodeTo).append("countryTo", countryTo).append("mapIdto", mapIdto).append("distance", distance).append("routes", routes).append("airlines", airlines).append("pnrCount", pnrCount).append("hasAirportChange", hasAirportChange).append("technicalStops", technicalStops).append("price", price).append("bagsPrice", bagsPrice).append("baglimit", baglimit).append("availability", availability).append("facilitatedBookingAvailable", facilitatedBookingAvailable).append("conversion", conversion).append("quality", quality).append("bookingToken", bookingToken).append("deepLink", deepLink).append("trackingPixel", trackingPixel).append("p1", p1).append("p2", p2).append("p3", p3).append("transfers", transfers).append("typeFlights", typeFlights).append("virtualInterlining", virtualInterlining).append("foundOn", foundOn).append("route", route).append("additionalProperties", additionalProperties).toString();
    }

}
