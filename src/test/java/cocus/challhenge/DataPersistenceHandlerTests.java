package cocus.challhenge;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.databind.JsonMappingException;

import cocus.flightinportugal.DataPersistenceHandler;
import cocus.flightinportugal.datamodel.Request;
import cocus.flightinportugal.datamodel.RequestRepository;


@DataMongoTest
@ExtendWith(SpringExtension.class)
public class DataPersistenceHandlerTests
{
	@Autowired
	static RequestRepository mongoDBRepository;
	
	private static DataPersistenceHandler dataPersistenceHandler;
	private static List<Request> reqs;

	@BeforeAll
	public static void setUp() throws com.fasterxml.jackson.core.JsonParseException, JsonMappingException, IOException 
	{
		dataPersistenceHandler = new DataPersistenceHandler(mongoDBRepository);
		reqs = TestingUtils.loadRequests("/requests.json");
	}
	
	@Test
	@Order(1)
	public void requestPersisting()
	{
		dataPersistenceHandler.persistRequests(reqs);
		List<Request> storedReqs = dataPersistenceHandler.getAllSearchRequests();
		Assertions.assertTrue(storedReqs.size()==reqs.size());
	}
	
	@Test
	@Order(2)
	public void specificReqRetrieval()
	{
		Request inputReq = reqs.get(3);
		Request storedReq = dataPersistenceHandler.getReqByID(inputReq.getId()).get();
		
		Assertions.assertTrue(storedReq.getId() == inputReq.getId());
		Assertions.assertTrue(storedReq.getDateFrom() == inputReq.getDateFrom());
		Assertions.assertTrue(storedReq.getDateTo() == inputReq.getDateTo());
		Assertions.assertTrue(storedReq.getDestinations().get(0) == inputReq.getDestinations().get(0));
		Assertions.assertTrue(storedReq.getDestinations().get(1) == inputReq.getDestinations().get(1));
		Assertions.assertTrue(storedReq.getCurrency() == inputReq.getCurrency());
	}
	
	@Test
	@Order(3)
	public void requestDeletion()
	{
		dataPersistenceHandler.deleteAllSearchRequests();
		List<Request> storedReqs = dataPersistenceHandler.getAllSearchRequests();
		Assertions.assertTrue(storedReqs==null || storedReqs.size()<1);
	}
	
}
