package cocus.challhenge;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import cocus.flightinportugal.FlightInPortugalController;


@SpringBootTest(classes = cocus.flightinportugal.FlightInPortugalApplication.class)
class FlightInPortugalApplicationTests
{

	@Autowired
	private FlightInPortugalController controller;

	//controller tests
	
	@Test
	public void contexLoads() throws Exception {assertThat(controller).isNotNull();}

}
