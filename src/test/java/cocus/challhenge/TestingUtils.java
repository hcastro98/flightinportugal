package cocus.challhenge;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cocus.flightinportugal.datamodel.Request;
import cocus.flightinportugal.msgmodel.ListReply;

public class TestingUtils
{	
	public static ListReply getReqListObj(String jsonFileName) throws com.fasterxml.jackson.core.JsonParseException, JsonMappingException, IOException
	{
		ListReply list;
		ObjectMapper mapper = new ObjectMapper();
		InputStream is = DataPersistenceHandlerTests.class.getResourceAsStream(jsonFileName);
		list = mapper.readValue(is, ListReply.class);
		return list;
	}
	
	public static List<Request> loadRequests(String fileName) throws com.fasterxml.jackson.core.JsonParseException, JsonMappingException, IOException 
	{
		return getReqListObj(fileName).getRequests();
	}
}
